var player = new Player();
let tab_draw = null;
window.onload = function() {
  var context = new AudioContext();
  Load();
}

function _getId(id) {
  return document.getElementById(id);
}
function _getClass(cls) {
  return document.getElementsByClassName(cls);
}
function _new(el) {
  return document.createElement(el);
}

function Load() {
  tab_draw = new Fingerboard('canvas');
  let c = new Chord('X33010');
  tab_draw.DrawChord(c);
  tab_draw.SetFrets(1, 5);
  DrawColorTable();
  onUpdateColor();
}

function Play() {
  var player = new Player();
  player.load_sounds([
    'sound.wav'
  ]);
  player.Play(0, 0);
}

function UpdateChord() {
  let ch = _getId('text_chord').value;
  let c = new Chord(ch);
  tab_draw.DrawChord(c);
}

function FlipOrientation() {
  tab_draw.FlipOrientation();
}
function FlipMirror() {
  tab_draw.FlipMirror();
}
function FlipLong() {
  tab_draw.FlipLong();
}
function DrawColorTable() {
  const notes = [ 'E', 'A', 'D', 'G', 'B', 'E' ];
  const colors = [ 'black', 'red', 'green', 'blue' ];
  const selected = [ 'black', 'red', 'green', 'blue', 'red', 'green' ];
  const t = _getId('tbl-color');
  for (index in notes) {
    let note = notes[index];
    let tr = _new('tr'),
        tdLabel = _new('td'),
        tdColor = _new('td');
    tdLabel.innerHTML = note;
    // Select color
    let s = _new('select');
    s.className = 'color-picker';
    for (color of colors) {
      let o = _new('option');
      o.innerHTML = color;
      if (color == selected[index]) {
        o.selected = 'selected';
      }
      s.appendChild(o);
    }
    s.addEventListener('change', onUpdateColor, false);
    tdColor.appendChild(s);
    tr.appendChild(tdLabel);
    tr.appendChild(tdColor);
    t.appendChild(tr);
  }
}

function onUpdateColor() {
  const colors = _getClass('color-picker');
  let color_values = [];
  for (color of colors) {
    color_values.push(color.value);
  }
  tab_draw.SetColors(color_values);
  UpdateChord();
};
