let div = document.getElementById('example3');
let canvas = document.createElement('canvas');
canvas.id = 'ex3-canvas';
div.appendChild(canvas);

let t = new Fingerboard(canvas.id);
t.SetColors([
  'red',
  'blue',
  'green',
  'yellow',
  'red',
  'purple']);
t.DrawChord(new Chord('X32010'));
