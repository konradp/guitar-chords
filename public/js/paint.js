let chord = null;
let color = 'black';
window.onload = function() {
  var context = new AudioContext();
  Load();
}

function _getId(id) {
  return document.getElementById(id);
}
function _getClass(cls) {
  return document.getElementsByClassName(cls);
}
function _new(el) {
  return document.createElement(el);
}

function Load() {
  chord = new Fingerboard('canvas');
  let c = new Chord([[], [], [], [], [], []]);
  chord.SetColors([[], [], [], [], [], []]);
  chord.DrawChord(c);
  FlipLong();
  FlipOrientation();
  chord.addCanvasEventListener('mousemove', onMouseMove);
  chord.addCanvasEventListener('click', onMouseClick);

  colorPicker = document.querySelector('#color');
  colorPicker.addEventListener('input', updateColor, false);
}

function onMouseMove(e) {
  let pos = this.MouseToFretboard(e);
  if (pos == null) {
    this.DrawChord(this.chord);
    return;
  }
  this.DrawChord(this.chord);
  let coord = this.CoordFinger(pos.string, pos.fret-this.fretStart+1);
  this.DrawCircleColor(coord.x, coord.y, true, color);
}
function onMouseClick(e) {
  let pos = this.MouseToFretboard(e);
  if (pos == null) return;
  let coord = this.CoordFinger(pos.string, pos.fret);
  let data = this.chord._data;
  let arr = data[pos.string-1];
  if (arr.includes(pos.fret)) {
    // Remove
    const idx = arr.indexOf(pos.fret);
    data[pos.string-1].splice(idx, 1);
    this.colors[pos.string-1].splice(idx, 1);
  } else {
    data[pos.string-1].push(pos.fret);
    this.colors[pos.string-1].push(color);
  }
  this.DrawChord(this.chord);
}
function updateColor(e) {
  color = e.target.value;
}


function UpdateChord() {
  let ch = _getId('text_chord').value;
  let c = new Chord(ch);
  chord.DrawChord(c);
}

function FlipOrientation() {
  chord.FlipOrientation();
}
function FlipMirror() {
  chord.FlipMirror();
};
function FlipLong() {
  const isLong = document.querySelector('#fliplong').checked;
  let fStart = document.querySelector('#fret_start');
  let fEnd = document.querySelector('#fret_end');
  if (isLong) {
    fStart.value = 1;
    fEnd.value = 22;
  } else {
    fStart.value = 1;
    fEnd.value = 5;
  }
  chord.FlipLong();
};
function FretsUpdate() {
  fretStart = parseInt(document.querySelector('#fret_start').value);
  fretEnd = parseInt(document.querySelector('#fret_end').value);
  chord.SetFrets(fretStart, fretEnd);
}
