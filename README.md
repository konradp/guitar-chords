# guitar-chords

demo: https://konradp.gitlab.io/guitar-chords/

![guitar-chords](image.png "guitar-chords")


```
npm install
npm start
```

Examples:

- draw chord: X23010
- draw single chord

# Components

- tab display
  - draw
  - data store
- strings data store

## Tab display


### Tab draw
C
```
  E A G D B E
0 X-----O---O
1 | | | | O |
  -----------
2 | | O | | |
  -----------
3 | O | | | |
  -----------
  | | | | | |
```

```
  4   3   2   1
|---|---|---|---O E
|---|---|---|-O-| B
|---|---|---|---O D
|---|---|-O-|---| G
|---|-O-|---|---| A
|---|---|---|---X E
```

## 
